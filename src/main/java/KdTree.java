import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class KdTree {

    private final Tree pointsTree = new Tree();

    public KdTree() {

    }                              // construct an empty set of points

    public boolean isEmpty() {
        return pointsTree.isEmpty();
    }// is the set empty?

    public int size() {
        return pointsTree.size;
    }// number of points in the set

    public void insert(Point2D p) {
        pointsTree.insert(p);

    }// add the point p to the set (if it is not already in the set)

    public boolean contains(Point2D p) {
        return pointsTree.contains(p);
    }// does the set contain the point p?

    public void draw() {
        for (Point2D point : pointsTree.getElements()) {
            point.draw();
        }
        pointsTree.draw();
    }// draw all of the points to standard draw

    public Iterable<Point2D> range(RectHV rect) {
        return pointsTree.range(rect);
    }// all points in the set that are inside the rectangle

    public Point2D nearest(Point2D p) {
        return pointsTree.nearest(p);
    }// a nearest neighbor in the set to p; null if set is empty

    /**
     * @author Slava Stashuk
     */
    private static class Tree {

        private int size;

        private static final Compare X = Compare.X;
        private static final Compare Y = Compare.Y;
        private Node root;

        private class Node {
            private final Point2D elem;
            private Node left, right;

            private Node(Point2D elem) {
                this.elem = elem;
            }
        }

        public Iterable<Point2D> getElements() {
            return getElements(root);
        }

        private List<Point2D> getElements(Node node) {
            if (node == null) return Collections.emptyList();
            List<Point2D> elems = new LinkedList<Point2D>();
            elems.add(node.elem);
            elems.addAll(getElements(node.left));
            elems.addAll(getElements(node.right));
            return elems;
        }

        public boolean isEmpty() {
            return root == null;
        }

        public void insert(Point2D elem) {
            if (root == null) {
                root = new Node(elem);
                size++;
            } else {
                if (put(root, elem, X, false)) size++;
            }
        }

        public boolean contains(Point2D elem) {
            if (root == null) return false;
            return !put(root, elem, X, true);
        }

        public Iterable<Point2D> range(RectHV rect) {
            if (root == null) return Collections.emptyList();
            return range(root, rect, X);
        }

        public Point2D nearest(Point2D point) {
            if (root == null) return null;
            return nearest(root, point, root.elem, new RectHV(0, 0, 1, 1), X);
        }

        public void draw() {
            draw(root, X);
        }

        private void draw(Node node, Compare compare) {
            StdDraw.setPenRadius();
            if (node == null) return;
            if (compare == X) {
                StdDraw.setPenColor(StdDraw.RED);
                StdDraw.line(node.elem.x(), 0, node.elem.x(), 1);
                draw(node.left, Y);
                draw(node.right, Y);
            } else {
                StdDraw.setPenColor(StdDraw.BLUE);
                StdDraw.line(0, node.elem.y(), 1, node.elem.y());
                draw(node.left, X);
                draw(node.right, X);
            }
        }

        private Point2D nearest(Node node, Point2D point, Point2D minPoint, RectHV rect, Compare compare) {
            double minDistance = minPoint.distanceSquaredTo(point);
            Point2D newMinPoint;
            if (node == null) return minPoint;
            if (rect.distanceSquaredTo(point) > minDistance) return minPoint;
            if (node.elem.distanceSquaredTo(point) < minDistance) {
                minPoint = node.elem;
            }
            if (compare == X) {
                double x = node.elem.x();
                if (point.x() <= x) {
                    newMinPoint = nearest(node.left, point, minPoint, pruneLeft(rect, x) , Y);
                    return nearest(node.right, point, newMinPoint, pruneRight(rect, x), Y);
                } else {
                    newMinPoint = nearest(node.right, point, minPoint, pruneRight(rect, x), Y);
                    return nearest(node.left, point, newMinPoint, pruneLeft(rect, x), Y);
                }
            } else {
                double y = node.elem.y();
                if (point.y() <= y) {
                    newMinPoint = nearest(node.left, point, minPoint, pruneDown(rect, y), X);
                    return nearest(node.right, point, newMinPoint, pruneUp(rect, y), X);
                } else {
                    newMinPoint = nearest(node.right, point, minPoint, pruneUp(rect, y), X);
                    return nearest(node.left, point, newMinPoint, pruneDown(rect, y), X);
                }
            }
        }

        private RectHV pruneLeft(RectHV rect, double xmax) {
            return new RectHV(rect.xmin(), rect.ymin(), xmax, rect.ymax());
        }

        private RectHV pruneRight(RectHV rect, double xmin) {
            return new RectHV(xmin, rect.ymin(), rect.xmax(), rect.ymax());
        }

        private RectHV pruneUp(RectHV rect, double ymin) {
            return new RectHV(rect.xmin(), ymin, rect.xmax(), rect.ymax());
        }

        private RectHV pruneDown(RectHV rect, double ymax) {
            return new RectHV(rect.xmin(), rect.ymin(), rect.xmax(), ymax);
        }

        private List<Point2D> range(Node node, RectHV rect, Compare compare) {
            if (node == null) return Collections.emptyList();
            List<Point2D> inRange = new LinkedList<Point2D>();
            if (rect.contains(node.elem)) inRange.add(node.elem);
            if (compare == X) {
                if (node.elem.x() <= rect.xmax()) {
                    inRange.addAll(range(node.right, rect, Y));
                }
                if (node.elem.x() >= rect.xmin()) {
                    inRange.addAll(range(node.left, rect, Y));
                }
            } else {
                if (node.elem.y() <= rect.ymax()) {
                    inRange.addAll(range(node.right, rect, X));
                }
                if (node.elem.y() >= rect.ymin()) {
                    inRange.addAll(range(node.left, rect, X));
                }
            }
            return inRange;
        }


        private boolean put(Node current, Point2D elem, Compare compare, boolean readOnly) {
            if (current.elem.equals(elem)) return false;
            if (compare == X) {
                if (elem.x() > current.elem.x()) {
                    if (current.right == null) {
                        if (!readOnly) current.right = new Node(elem);
                    } else {
                        return put(current.right, elem, Y, readOnly);
                    }
                } else {
                    if (current.left == null) {
                        if (!readOnly) current.left = new Node(elem);
                    } else {
                        return put(current.left, elem, Y, readOnly);
                    }
                }
            } else {
                if (elem.y() > current.elem.y()) {
                    if (current.right == null) {
                        if (!readOnly) current.right = new Node(elem);
                    } else {
                        return put(current.right, elem, X, readOnly);
                    }
                } else {
                    if (current.left == null) {
                        if (!readOnly) current.left = new Node(elem);
                    } else {
                        return put(current.left, elem, X, readOnly);
                    }
                }
            }
            return true;
        }

        private enum Compare {
            X, Y
        }
    }
}