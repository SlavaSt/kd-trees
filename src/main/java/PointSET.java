import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

public class PointSET {

    private final TreeSet<Point2D> points = new TreeSet<Point2D>();

    public PointSET() {

    }                              // construct an empty set of points

    public boolean isEmpty() {
        return points.isEmpty();
    }// is the set empty?

    public int size() {
        return points.size();
    }// number of points in the set

    public void insert(Point2D p) {
        points.add(p);
    }// add the point p to the set (if it is not already in the set)

    public boolean contains(Point2D p) {
        return points.contains(p);
    }// does the set contain the point p?

    public void draw() {
        for (Point2D point : points) {
            point.draw();
        }
    }// draw all of the points to standard draw


    public Iterable<Point2D> range(RectHV rect) {
        List<Point2D> inRange = new LinkedList<Point2D>();
        for (Point2D point : points) {
            if (rect.contains(point)) {
                inRange.add(point);
            }
        }
        return inRange;
    }// all points in the set that are inside the rectangle

    public Point2D nearest(Point2D p) {
        if (isEmpty()) return null;
        Point2D nearest = points.first();
        double minDistance = p.distanceSquaredTo(nearest);
        for (Point2D point : points) {
            double distance = p.distanceSquaredTo(point);
            if (distance < minDistance) {
                minDistance = distance;
                nearest = point;
            }
        }
        return nearest;
    }// a nearest neighbor in the set to p; null if set is empty
}